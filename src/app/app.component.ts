import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';

import {isNil} from 'lodash';
import {Observable, Subject} from 'rxjs';

import {ListItemService} from './app-common/list-item/list-item.service';
import {ListItem} from './app-common/list-item/list-item';
import {ListItemType} from './app-common/list-item/list-item-type.enum';
import {AppListItemFilters} from './app-list-item-filters';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  public listItems$: Observable<ListItem[]>;
  public formGroup: FormGroup;

  public ListItemType = ListItemType;

  private isListInAscendingOrder;

  private destroySubject: Subject<void> = new Subject();

  constructor(private formBuilder: FormBuilder,
              private listItemService: ListItemService) {
  }

  public get listItemTypeControl(): FormControl {
    return this.formGroup.get('type') as FormControl;
  }

  public get listItemTitleControl(): FormControl {
    return this.formGroup.get('title') as FormControl;
  }

  public ngOnInit(): void {
    this.getListItems();
    this.createForm();
  }

  public ngOnDestroy(): void {
    this.destroySubject.next();
  }

  public trackByFn(index: number, listItem: ListItem): ListItemType {
    return listItem.type;
  }

  public getListItems(): void {
    this.listItems$ = this.listItemService.listItems$;
    this.listItemService.fetchListItems();
  }

  public resetFilters(): void {
    this.formGroup.reset();
    this.listItemService.resetListItemFilters();
  }

  public sortItems(): void {
    this.isListInAscendingOrder = !!!this.isListInAscendingOrder;
    const sortOrder = this.isListInAscendingOrder ? 'asc' : 'desc';
    this.listItemService.sortListItems(sortOrder);
  }

  private createForm(): void {
    this.formGroup = this.formBuilder.group({
      type: this.formBuilder.control(null),
      title: this.formBuilder.control(null)
    });

    this.listItemTypeControl.valueChanges
      .pipe(takeUntil(this.destroySubject))
      .subscribe((listItemType: ListItemType) => this.listItemService
        .applyListItemFilter('type', AppListItemFilters.filterByListItemType(listItemType)));

    this.listItemTitleControl.valueChanges
      .pipe(takeUntil(this.destroySubject))
      .subscribe((listItemTitle: string) => this.listItemService
        .applyListItemFilter('title', AppListItemFilters.filterByListItemTitle(listItemTitle)));
  }
}
