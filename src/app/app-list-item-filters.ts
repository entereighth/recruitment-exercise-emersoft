import {isNil} from 'lodash';

import {ListItemFilterFn} from './app-common/list-item/list-item.service';
import {ListItemType} from './app-common/list-item/list-item-type.enum';
import {ListItem} from './app-common/list-item/list-item';

export class AppListItemFilters {

  public static filterByListItemType(listItemType: ListItemType): ListItemFilterFn {
    return ((listItem: ListItem) => isNil(listItemType) ? true : listItem.type === listItemType);
  }

  public static filterByListItemTitle(listItemTitle: string): ListItemFilterFn {
    return (listItem => isNil(listItemTitle) || listItemTitle === ''
      ? true
      : !!listItem.title.match(new RegExp(listItemTitle, 'gi')));
  }
}
