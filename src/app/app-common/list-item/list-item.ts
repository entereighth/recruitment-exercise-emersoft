import {ListItemType} from './list-item-type.enum';

export class ListItem {
  title: string;
  type: ListItemType;
}
