import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {ListItem} from './list-item';
import {LIST_ITEMS} from '../../mock-list-item';
import {delay, map} from 'rxjs/operators';
import {isEqual} from 'lodash';

type ListItemFilterType = 'type' | 'title';

interface ListItemFilters {
  [key: string]: ListItemFilterFn;
}

export type ListItemFilterFn = (listItem: ListItem) => boolean;

@Injectable({
  providedIn: 'root'
})
export class ListItemService {

  private listItemsSubject: BehaviorSubject<ListItem[]> = new BehaviorSubject([]);
  public listItems$: Observable<ListItem[]> = this.listItemsSubject.asObservable()
    .pipe(
      delay(500),
      map(listItems => this.filterListItems(listItems) || [])
    );
  private listItemFiltersSubject: BehaviorSubject<ListItemFilters> = new BehaviorSubject({});
  private listItemFilters$: Observable<ListItemFilters> = this.listItemFiltersSubject.asObservable();

  constructor() {
    this.listItemFilters$
      .pipe()
      .subscribe(() => {
        this.reloadListItems();
      });
  }

  public fetchListItems() {
    this.setListItems(LIST_ITEMS);
  }

  public reloadListItems() {
    this.setListItems(this.currentListItems);
  }

  public applyListItemFilter(listItemFilterType: ListItemFilterType, listItemFilterFn: ListItemFilterFn): void {
    const newFilters = {...this.currentFilters};
    newFilters[listItemFilterType] = listItemFilterFn;
    this.setListItemFilters(newFilters);
  }

  public resetListItemFilters(): void {
    this.setListItemFilters({});
  }

  public sortListItems(order: 'asc' | 'desc'): void {
    const newListItems = [...this.currentListItems]
      .sort((item1, item2) => (item1.title > item2.title) ? 1 : -1);

    if (order === 'desc') {
      newListItems.reverse();
    }
    this.setListItems(newListItems);
  }

  private get currentFilters(): ListItemFilters {
    return this.listItemFiltersSubject.getValue();
  }

  private get currentListItems(): ListItem[] {
    return this.listItemsSubject.getValue();
  }

  private setListItemFilters(listItemFilters: ListItemFilters): void {
    this.listItemFiltersSubject.next(listItemFilters);
  }

  private setListItems(listItems: ListItem[]): void {
    this.listItemsSubject.next(listItems);
  }

  private filterListItems(listItems: ListItem[]): ListItem[] {
    const filters = Object.values(this.currentFilters);
    if (filters && filters.length > 0) {
      return listItems.filter(listItem => {
        return filters
          .map(filterFn => filterFn(listItem))
          .reduce((prev, curr) => prev && curr);
      });
    }
    return listItems;
  }
}
