import {ListItem} from './app-common/list-item/list-item';
import {ListItemType} from './app-common/list-item/list-item-type.enum';

export const LIST_ITEMS: ListItem[] = [
  {
    type: ListItemType.Audio,
    title: 'Recording'
  },
  {
    type: ListItemType.Audio,
    title: 'Podcast'
  },
  {
    type: ListItemType.Audio,
    title: 'Song'
  },
  {
    type: ListItemType.Document,
    title: 'Agreement'
  },
  {
    type: ListItemType.Document,
    title: 'Bill'
  },
  {
    type: ListItemType.Image,
    title: 'Wallpaper'
  },
  {
    type: ListItemType.Image,
    title: 'Thumbnail'
  },
  {
    type: ListItemType.Image,
    title: 'Icon'
  },
  {
    type: ListItemType.Video,
    title: 'Commercial'
  },
  {
    type: ListItemType.Video,
    title: 'Movie'
  },
  {
    type: ListItemType.Video,
    title: 'TV Series Episode'
  },
  {
    type: ListItemType.Video,
    title: 'Musical'
  }
];
